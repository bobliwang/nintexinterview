﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json;
using NintexInterview.Common.ViewModels;

namespace NintexInterview.Common.Utils.Test
{
  [TestClass]
  public class HttpServiceTest
  {

    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    private const string ExternalServiceEndPoint = "http://nmflightservice.cloudapp.net/api/Flight";

    [TestMethod]
    public void HttpService_SendRequestToExternalFlightInfoService_ShouldReturnFlightInfoList()
    {
      var json = new HttpService().SendGetRequest(ExternalServiceEndPoint,
        paramName: "filter",
        payload: new FlightSearchRequest
        {
          DepartureAirportCode = "MEL",
          ArrivalAirportCode = "LHR",
          DepartureDate = DateTime.Now.AddDays(-2),
          ReturnDate = DateTime.Now.AddDays(-1)
        });

      var list = JsonConvert.DeserializeObject<List<FlightInfo>>(json);

      logger.Info("Received data:" + json);

      Assert.IsNotNull(list);
    }
  }
}
