﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using NintexInterview.Common.ModelBinder;
using NintexInterview.Common.ViewModels;
using NintexInterview.Services;
using NintexInterview.Services.Interfaces;

namespace NintexInterview.WebApp.Controllers.Api
{
  public class ApiFlightProxyController : ApiController
  {
    private readonly IFlightService _flightService;

    public ApiFlightProxyController(IFlightService flightService)
    {
      _flightService = flightService;
    }

    [HttpGet]
    [FromJsonQueryString]
    public IList<FlightInfo> GetFlightInfos(FlightSearchRequest filter)
    {
      return _flightService.Search(filter);
    }
  }
}