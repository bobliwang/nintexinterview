﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Security;
using System.Web.SessionState;
using Autofac;
using Autofac.Integration.WebApi;
using log4net;

namespace NintexInterview.WebApp
{
  public class Global : System.Web.HttpApplication
  {
    private static readonly ILog Logger;

    static Global()
    {
      log4net.Config.XmlConfigurator.Configure();
      Logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    }



    protected void Application_Start(object sender, EventArgs e)
    {
      var builder = new ContainerBuilder();

      var dependencyConfig = new DependencyConfig();
      var container = dependencyConfig.RegisterDependncies(builder);


      // Configure Web API with the dependency resolver.
      var webApiResolver = new AutofacWebApiDependencyResolver(container);
      GlobalConfiguration.Configuration.DependencyResolver = webApiResolver;


      WebApiConfig.Register(GlobalConfiguration.Configuration);
    }

    protected void Session_Start(object sender, EventArgs e)
    {

    }

    protected void Application_BeginRequest(object sender, EventArgs e)
    {

    }

    protected void Application_AuthenticateRequest(object sender, EventArgs e)
    {

    }

    protected void Application_Error(object sender, EventArgs e)
    {

    }

    protected void Session_End(object sender, EventArgs e)
    {

    }

    protected void Application_End(object sender, EventArgs e)
    {

    }
  }
}