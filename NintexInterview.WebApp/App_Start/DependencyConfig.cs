﻿using System.Configuration;
using Autofac;
using Autofac.Integration.WebApi;
using NintexInterview.Common.Utils;
using NintexInterview.Services;
using NintexInterview.Services.Interfaces;

namespace NintexInterview.WebApp
{
  public class DependencyConfig
  {
    private const string AppSettingKey_ExternalFlightServiceEndPoint = "ExternalFlightServiceEndPoint";

    public IContainer RegisterDependncies(ContainerBuilder builder)
    {
      builder.RegisterType<HttpService>()
             .As<IHttpService>()
             .InstancePerRequest();
       
      builder.Register(x =>
      {
        var endPointUrl = ConfigurationManager.AppSettings[AppSettingKey_ExternalFlightServiceEndPoint];
        var httpService = x.Resolve<IHttpService>();

        return new FlightProxyService(endPointUrl, httpService);
      }).As<IFlightService>().InstancePerRequest();

      var globalAssembly = typeof(Global).Assembly;
      builder.RegisterApiControllers(globalAssembly);

      var container = builder.Build();

      return container;
    }
  }
}