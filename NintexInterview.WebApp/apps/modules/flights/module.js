﻿// the module
angular.module('flights', ['my.ui.datepicker', 'NgSwitchery', 'ngRoute', 'ui.bootstrap'])
  // inject flightInfoProvider.
  .factory('flightInfoProvider', function ($http) {
    return {
      getFlightInfos : function(url, flightSearchRequest) {
        return $http.get(url, {
          cache: false, // no cache
          params: { filter: flightSearchRequest }
        });
      }
    };
  })
  // inject flightService here.
  .factory('flightService', function ($http, $log, flightInfoProvider) {
    return {
      getFlightInfos: function (flightSearchRequest, useCORS) {
        var url = useCORS ? 'http://nmflightservice.cloudapp.net/api/Flight' : '/api/FlightProxy';

        $log.info("url:" + url);

        return flightInfoProvider.getFlightInfos(url, flightSearchRequest);
      }

    };
  })
  .factory('flightSearchRequestValidator', function() {
    return {
      validate: function (flightSearchRequest, errors) {
        // validate departureAirportCode
        var departureAirportCode = flightSearchRequest.DepartureAirportCode;
        if (departureAirportCode != null && departureAirportCode.length != 3) {

          errors.push('Departure airport code should have 3 characters.');

          return false;
        }

        // validate arrivalAirportCode
        var arrivalAirportCode = flightSearchRequest.ArrivalAirportCode;
        if (arrivalAirportCode != null && arrivalAirportCode.length != 3) {

          errors.push('Arrival airport code should have 3 characters.');
          return false;
        }

        // validate departureDate
        var departureDate = flightSearchRequest.DepartureDate;
        if (departureDate == null && !angular.isDate(departureDate)) {

          errors.push('Departure date is not a valid date.');
          return false;
        }

        // validate returnDate
        var returnDate = flightSearchRequest.ReturnDate;
        if (returnDate == null && !angular.isDate(returnDate)) {

          errors.push('Return date is not a valid date.');
          return false;
        }

        return true;
      }
    };
  })
  // routing config.
  .config(function ($routeProvider, $locationProvider) {
    $routeProvider.when('/', {
      controller: 'FlightCtrl',
      templateUrl: 'list.cshtml'
    }).otherwise({
      redirectTo: '/'
    });
  })
  // controller
  .controller('FlightCtrl', function ($scope, $http, $timeout, $log, flightService, flightSearchRequestValidator) {

    // initializer
    function init() {
      var today = new Date();
      today.setDate(today.getDate()); // remove time part.

      $scope.flightSearchRequest = { DepartureDate: today };
      $scope.flightSearchRequest.ReturnDate = new Date();
      $scope.flightSearchRequest.ReturnDate.setDate(today.getDate() + 14);

      $scope.serviceType = { useCORS: false };
      $scope.filterStatus = { showFilter: true };
      $scope.validationErrors = [];
    }

    // get flight info.
    $scope.getFlightInfos = function () {

      if (!validate()) {
        return;
      }

      // prepare to call service.
      preSendSearchRequest();

      flightService.getFlightInfos($scope.flightSearchRequest, $scope.serviceType.useCORS)
        .success(function(data, status, headers, config) {
          onSearchSuccess(data, status, headers, config);
        })
        .error(function (data, status, headers, config) {
          onSearchError(data, status, headers, config);
        })
      ;
    };

    // parameter validation
    function validate() {

      $scope.validationErrors = [];
      if (!flightSearchRequestValidator.validate($scope.flightSearchRequest, $scope.validationErrors)) {
        return false;
      }

      return true;
    }

    // callback for a successful search
    function onSearchSuccess(data, status, headers, config) {
      $scope.flightInfos = data;
      $scope.isSearching = false;
    }

    // callback for a failed search
    function onSearchError(data, status, headers, config) {
      $scope.isSearching = false;
      $scope.filterStatus.showFilter = true;

      $scope.errorMsg = "Sorry but we cannot retrieve flights info.";

      $log.error("error", {
        data: data,
        status: status,
        headers: headers,
        config: config
      });
    }

    // prepare search
    function preSendSearchRequest() {

      $scope.flightInfos = [];

      // clear error msg
      $scope.errorMsg = null;
      
      // hide filter after search
      $scope.filterStatus.showFilter = false;

      // set isSearching to true - disable the search button
      $scope.isSearching = true;
    }


    // call initalizer
    init();
});