﻿angular.module('my.ui.datepicker', ['ngRoute', 'ui.bootstrap'])
  .directive("myUiDatepicker", function($timeout) {
    return {
      restrict: 'E',

      scope: {
        model: "=",
      },

      link: function (scope, element, attrs) {
        scope.inputId = attrs["inputId"];
      },

      controller: function ($scope) {
        $scope.open = function () {
          $timeout(function() {
            $scope.isOpen = true;
          });
        };
      },

      templateUrl: '/apps/components/datepicker/datepicker.cshtml'
    };
  });
  
