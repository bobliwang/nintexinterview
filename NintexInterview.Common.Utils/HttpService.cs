﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

using System.Text;
using System.Threading.Tasks;
using System.Web;
using log4net;
using Newtonsoft.Json;

namespace NintexInterview.Common.Utils
{
  public interface IHttpService
  {
    /// <summary>
    /// Send HTTP GET request and  read the response from server as a string.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="contentType"></param>
    /// <param name="paramName"></param>
    /// <param name="payload"></param>
    /// <returns></returns>
    string SendGetRequest(string url, string contentType = "application/json", string paramName = null, object payload = null);
  }

  public class HttpService : IHttpService
  {
    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    /// <summary>
    /// Send HTTP GET request and  read the response from server as a string.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="contentType"></param>
    /// <param name="paramName"></param>
    /// <param name="payload"></param>
    /// <returns></returns>
    public string SendGetRequest(string url, string contentType = "application/json", string paramName = null, object payload = null)
    {
      var request = this.PrepareGetRequest(url, contentType, paramName, payload);

      try
      {
        var response = request.GetResponse();

        return this.ReadFromResponse(response);
      }
      catch (WebException webEx)
      {
        if (webEx.Response == null)
        {
          throw;
        }

        throw this.ConvertWebException(webEx.Response, webEx);
      }
    }

    /// <summary>
    /// Read server response.
    /// </summary>
    /// <param name="response"></param>
    /// <returns></returns>
    private string ReadFromResponse(WebResponse response)
    {
      // grab the response
      using (var responseStream = response.GetResponseStream())
      {
        using (var reader = new StreamReader(responseStream))
        {
          return reader.ReadToEnd();
        }
      }
    }

    /// <summary>
    /// Prepare the HTTP GET request.
    /// </summary>
    /// <param name="url"></param>
    /// <param name="contentType"></param>
    /// <param name="paramName"></param>
    /// <param name="payload"></param>
    /// <returns></returns>
    private HttpWebRequest PrepareGetRequest(string url, string contentType, string paramName, object payload)
    {
      if (!string.IsNullOrEmpty(paramName))
      {
        var json = payload != null ? JsonConvert.SerializeObject(payload) : "";
        url += "?" + string.Format("{0}={1}", paramName, HttpUtility.UrlEncode(json));
      }

      logger.Info("url:" + url);

      var request = WebRequest.CreateHttp(url);

      request.Method = "GET";
      request.ContentLength = 0;
      request.ContentType = contentType;
      request.KeepAlive = true;
      return request;
    }

    private Exception ConvertWebException(WebResponse errorResponse, WebException webEx)
    {
      var errorFromServer = this.ReadFromResponse(errorResponse);
      return new Exception(errorFromServer, webEx);
    }
  }
}
