﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace NintexInterview.Common.ViewModels
{
  /// <summary>
  /// Collecting following information from the user to search for flights.
  /// </summary>
  public class FlightSearchRequest
  {
    /// <summary>
    /// Departure airport code: a three character string.
    /// Characters can be any alphanumeric value and does not have to be a valid airport code.
    /// This field does not need to support auto complete or suggestion.
    /// </summary>
    [StringLength(maximumLength:3, MinimumLength = 3)]
    public string DepartureAirportCode { get; set; }

    /// <summary>
    /// Arrival airport code: a three character string.
    /// Characters can be any alphanumeric value and does not have to be a valid airport code.
    /// This field does not need to support auto complete or suggestion.
    /// </summary>
    [StringLength(maximumLength: 3, MinimumLength = 3)]
    public string ArrivalAirportCode { get; set; }

    /// <summary>
    /// Departure date: a date only field.
    /// </summary>
    public DateTime DepartureDate { get; set; }

    /// <summary>
    /// Departure date: a date only field.
    /// </summary>
    public DateTime ReturnDate { get; set; }
  }
}
