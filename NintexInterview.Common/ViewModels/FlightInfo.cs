﻿using System;

namespace NintexInterview.Common.ViewModels
{
  public class FlightInfo
  {
    /// <summary>
    /// Airline logo url
    /// </summary>
    public string AirlineLogoAddress { get; set; }

    /// <summary>
    /// Airline name
    /// </summary>
    public string AirlineName { get; set; }

    /// <summary>
    /// Inbound flight duration
    /// </summary>
    public string InboundFlightsDuration { get; set; }

    /// <summary>
    /// Outbound flight duration
    /// </summary>
    public string OutboundFlightsDuration { get; set; }

    /// <summary>
    /// Total amount in USD.
    /// </summary>
    public double TotalAmount { get; set; }

    public string ItineraryId { get; set; }

    public int Stops { get; set; }
  }
}