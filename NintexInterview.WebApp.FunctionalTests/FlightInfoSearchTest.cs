﻿using System;
using System.IO;
using log4net;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NintexInterview.WebApp.FunctionalTests.Helpers;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Remote;

namespace NintexInterview.WebApp.FunctionalTests
{
  [TestClass]
  public class FlightInfoSearchTest
  {
    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    /// <summary>
    /// The url base to local iis express web app folder.
    /// </summary>
    private readonly string _urlBase = "http://localhost:2753/apps";

    private ChromeDriver _chrome;

    [TestMethod]
    public void SearchForFlights_WithValidParameter_ShouldSucceed()
    {
      using (_chrome = new ChromeDriver())
      {
        var element = _chrome.Helper(_urlBase)
          .GoToModule("flights")
          .SetTextInNgModel("MEL", "flightSearchRequest.DepartureAirportCode")
          .SetTextInNgModel("LHY", "flightSearchRequest.ArrivalAirportCode")
          .ClickByNgClick("getFlightInfos()")
          .FindElementByXPath("//div[@id='divResultsSummary']");

        Assert.IsNotNull(element);
      }
    }

    [TestMethod]
    public void SearchForFlights_WithTooLongDepartureAirportCode_ShouldResultInValidationFailure()
    {
      using (_chrome = new ChromeDriver())
      {
        var element = _chrome.Helper(_urlBase)
          .GoToModule("flights")
          .SetTextInNgModel("MEL1", "flightSearchRequest.DepartureAirportCode")
          .SetTextInNgModel("LHY", "flightSearchRequest.ArrivalAirportCode")
          .ClickByNgClick("getFlightInfos()")
          .FindElementByXPath("//p[contains(.,'Departure airport code should have 3 characters')]");

        Assert.IsNotNull(element);
      }
    }

    [TestMethod]
    public void SearchForFlights_WithTooShortDepartureAirportCode_ShouldResultInValidationFailure()
    {
      using (_chrome = new ChromeDriver())
      {
        var element = _chrome.Helper(_urlBase)
          .GoToModule("flights")
          .SetTextInNgModel("M", "flightSearchRequest.DepartureAirportCode")
          .SetTextInNgModel("LHY", "flightSearchRequest.ArrivalAirportCode")
          .ClickByNgClick("getFlightInfos()")
          .FindElementByXPath("//p[contains(.,'Departure airport code should have 3 characters')]");

        Assert.IsNotNull(element);
      }
    }

    [TestMethod]
    public void SearchForFlights_WithInvalidDepartureDate_ShouldResultInValidationFailure()
    {
      using (_chrome = new ChromeDriver())
      {
        var helper = _chrome.Helper(_urlBase);

          helper.GoToModule("flights")
          .SetTextInNgModel("MEL", "flightSearchRequest.DepartureAirportCode")
          .SetTextInNgModel("LHY", "flightSearchRequest.ArrivalAirportCode");
          
          helper.FindElementByXPath("//input[@id='txtDepartureDate']").SendKeys("10/12/2014BBB");

          var element = helper.ClickByNgClick("getFlightInfos()")
          .FindElementByXPath("//p[contains(.,'Departure date is not a valid date')]");

        Assert.IsNotNull(element);
      }
    }
  }
}
