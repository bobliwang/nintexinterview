using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using NintexInterview.Common.ViewModels;
using NintexInterview.Services.Interfaces;

namespace NintexInterview.Services
{
  public class SampleFlightService : IFlightService
  {
    public IList<FlightInfo> Search(FlightSearchRequest filter)
    {
      return JsonConvert.DeserializeObject<List<FlightInfo>>(File.ReadAllText(@"C:\Users\liwang\Documents\gitrepo\NintexInterview\NintexInterview.WebApp\App_Data\sample.json"));
    }
  }
}