﻿using System;
using System.Collections.Generic;
using log4net;
using log4net.Repository.Hierarchy;
using Newtonsoft.Json;
using NintexInterview.Common.Utils;
using NintexInterview.Common.ViewModels;
using NintexInterview.Services.Interfaces;

namespace NintexInterview.Services
{
  public class FlightProxyService : IFlightService
  {
    /// <summary>
    /// The url to external web service. e.g: "http://nmflightservice.cloudapp.net/api/Flight"
    /// </summary>
    private readonly string _serviceEndPoint;

    private readonly IHttpService _httpService;

    private static readonly ILog logger = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

    public FlightProxyService(string serviceEndPoint, IHttpService httpService)
    {
      _serviceEndPoint = serviceEndPoint;
      _httpService = httpService;
    }

    public IList<FlightInfo> Search(FlightSearchRequest filter)
    {
      try
      {
        var result = _httpService.SendGetRequest(_serviceEndPoint, paramName: "filter", payload: filter);

        return JsonConvert.DeserializeObject<List<FlightInfo>>(result);
      }
      catch (Exception ex)
      {
        logger.Error("Failed to get flight infos. REASON: " + ex.Message, ex);
        throw;
      }
    }
  }
}