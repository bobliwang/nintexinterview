﻿using System.Collections.Generic;
using NintexInterview.Common.ViewModels;

namespace NintexInterview.Services.Interfaces
{
  public interface IFlightService
  {
    IList<FlightInfo> Search(FlightSearchRequest filter);
  }
}